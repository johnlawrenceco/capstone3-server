const jwt = require('jsonwebtoken') //json web token
const secret = "CourseBookingAPI" // secret can be any phrase

// JWT Functionality 

//Create Access Token -> pack gift and sign it with the secret.
	
module.exports.createAccessToken = (user) => {
	//use parameter will be provided from our login.

	const data ={

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {}) //data - to be sent , secret - id
	
}

//verify the token 

module.exports.verify = (req, res, next) => {
	//put JWT in the header of request and send a response to the client.
let token = req.headers.authorization 
// This is in postman as: Authorization -> Bearer Token -> Token
// console.log(token)



	//(to check if token exists)
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)
		// <string>.slice - cut the string to the specified value.
		// start at the 7th and end at the last character.
		// The first 7 characters are not related to the actual data we need.

		return jwt.verify(token, secret, (err, data) => {
			// if there is an error in the verification of the token, the verification fails.
			return (err) ? res.send ({auth: "failed"}) : next()
			// next() is a function that allows to proceed to the next request
		})
	} else {
		//if token is empty
		return res.send({auth: "failed"})
	}

}

// decode the token -> open the gift and unraveling the contents
module.exports.decode = (token) => { // pass token from the route

	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err,data) => {
			return (err) ? null : jwt.decode(token, {complete:true}).payload
			//jwt.decode - decodes our token and gets the payload.
			// payload - passed data from createAccessToken function
		})
	} else {
		return null 
	}
}

