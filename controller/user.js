const User = require('../model/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = '204446368530-hec7o84r89pvtrcdq61ff215pn3tau9g.apps.googleusercontent.com'
const { google } = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const nodemailer = require('nodemailer')
require('dotenv').config()



module.exports.register = (params) => {
	let newUser = new User({

		firstName: params.firstName, 
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)

	})

	return newUser.save().then((user, err)=> {
		return (err) ?  false : true 
	})
}

module.exports.emailExists = (params) => {

	return User.find({ email: params.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})

}

module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne == null) {
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)

		if(isPasswordMatched) {
			return {accessToken: auth.createAccessToken(resultFromFindOne)}
		} else {
			return false
		}
	})

}

module.exports.get = (params) => {

	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}


module.exports.newTransaction = (body, params) => {           
	console.log(params.userId)
	return User.findById(params.userId).then(resultFromFindById => {
		console.log(resultFromFindById)
		 resultFromFindById.transactions.push({categoryType: body.categoryType,categoryName: body.categoryName,amount: body.amount,description: body.description})
			return resultFromFindById.save().then((transaction, err) => {
				console.log(transaction)
				return (err) ? false: true
 			})
	})
	
}

module.exports.newCategory = (body, params) => { 
	return User.findById(params.userId).then(resultFromFindById => {
<<<<<<< HEAD
		 resultFromFindById.categories.push({categoryType: body.categoryType, categoryName: body.categoryName})
		 console.log(resultFromFindById)
=======
		 resultFromFindById.categories.push(newCategory)
>>>>>>> 9c9f6a1cea163883c345c5fc54660434c1efc95a
			return resultFromFindById.save().then((transaction, err) => {
				return (err) ? false: true
 			})
	})                  
}

module.exports.getAllCategories = (params) => { 
	return User.findById(params.userId).then(resultFromFindById =>  {
		return resultFromFindById.categories
	})
}

module.exports.getAllTransactions = (params) => { 
	return User.findById(params.userId).then(resultFromFindById => {
		return resultFromFindById.transactions
	})
}


module.exports.verifyGoogleTokenId = async (tokenId, accessToken) => {

	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})

	if(data.payload.email_verified === true) {
		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null) {
			 console.log('A user with the same email has been registered.')
			 if (user.loginType === 'google') {
			 	return {accessToken: auth.createAccessToken(user)}
			 } else {
			 	return{error: 'login-type-error' }
			 }
		} else {
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'

			})
			return newUser.save().then((user, err) => {
				const mailOptions = {
					from: 'joerenlee1@gmail.com',
					to: user.email,
					subject: 'Thank you for registering to Zuitt Booking.',
					text: `You are registered to Zuitt Booking on ${new Date().toLocaleString()}`,
					html: `You registered to Zuitt Booking on ${new Date().toLocaleString()}`
				}

				const transporter = nodemailer.createTransport({
					host: 'smtp.gmail.com',
					port: 465,
					secure: true,
					auth: {

						type: 'OAuth2',
						user: process.env.MAILING_SERVICE_EMAIL,
						clientId: process.env.MAILING_SERVICE_CLIENT_ID,
						clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
						refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
						accessToken: accessToken

					}
				})
				function sendMail(transporter){
					transporter.sendMail(mailOptions, function(err, result) {
						if(err) {
							console.log(err)
						} else if(result) { 
							transport.close()
						}
					})
				}

				sendMail(transporter)

				return {accessToken: auth.createAccessToken(user)}

			})
		}
	} else {
			return {error: 'google-auth-error'}
	}
}