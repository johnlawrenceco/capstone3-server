const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')


app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())

mongoose.connection.once('open', () => console.log('Connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://jlcco:jlcco1234@cluster0.0krgn.mongodb.net/budget-tracker?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
})


const userRoutes = require('./route/user')

app.use('/api/users', userRoutes)
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on ${process.env.PORT || 4000}`)
})

