const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({ 
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String
	},
	mobileNo: {
		type: String
	},
	loginType: {
		type: String
	},
	transactions: [
		{
			categoryType: {
					type: String,
					required: [true, 'Category Type is required.']
			},
			categoryName: {
					type: String, //BSON
					required: [true, 'Category Name is required.']
			},
			amount: {
				type: Number,
				required: [true, 'Amount is required.']
			},
			description: {
				type: String,
				required: [true, 'Description is required.']
			},
			transactionDate: {
				type: Date,
				default: new Date()
			}
		}
	],
	categories: [
		{
			categoryType: {
					type: String,
					required: [true, 'Category Type is required.']
			},
			categoryName: {
					type: String, //BSON
					required: [true, 'Category Name is required.']
			}
		}
	]
})

module.exports = mongoose.model('user', userSchema) 