const User = require('../model/User')
const UserController = require('../controller/user')
const express = require('express')
const router = express.Router()
const auth = require('../auth')


router.post('/', (req,res) => {
	
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

router.get('/details', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization) 
	UserController.get({userId: user.id}).then(user => res.send(user)) 
})


router.post('/transactions/new', auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization)

	let params = {
		userId: user.id
	}
	console.log(req.body)
	UserController.newTransaction(req.body, params).then(resultFromNewTransaction => res.send(resultFromNewTransaction))
})

router.post('/categories/new', auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization)

	let params = {
		userId: user.id
	}
	UserController.newCategory(req.body, params).then(resultFromNewCategory => res.send(resultFromNewCategory))
})

router.get('/categories', auth.verify, (req,res) => {
	let user = auth.decode(req.headers.authorization)

	let params = {
		userId: user.id
	}
	UserController.getAllCategories(params).then(resultFromGetAllCategories => res.send(resultFromGetAllCategories))
})

router.get('/transactions', auth.verify, (req,res) => {
	let user = auth.decode(req.headers.authorization)

	let params = {
		userId: user.id
	}
	
	UserController.getAllTransactions(params).then(resultFromGetAllTransactions => res.send(resultFromGetAllCategories))
})

router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId, req.body.accessToken))
})

module.exports = router